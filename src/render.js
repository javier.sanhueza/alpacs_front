import Viva from 'vivagraphjs'

const gradiant = [
  [
    0,
    [0, 0, 0] // black
  ],
  [
    20,
    [255, 0, 0] // red
  ],
  [
    50,
    [255, 127.5, 0] // orange
  ],
  [
    80,
    [255, 255, 0] // yellow
  ],
  [
    100,
    [231, 231, 255] // lightgray
  ]
]

// attr value between [0, 100]
export function colorRange (value) {
  if (value !== undefined && value >= 0) {
    const i = gradiant.findIndex((color, index) => value < color[0] || (value <= color[0] && index > 0))
    if (i > 0 && i < gradiant.length) {
      // const color1 = gradiant[i - 1][1]
      // const color2 = gradiant[i][1]
      const color1X = value - gradiant[i - 1][0]
      const color2X = gradiant[i][0] - value
      const ratio = color2X / (color1X + color2X)
      return pickHex(gradiant[i - 1][1], gradiant[i][1], ratio)
    }
  }
  return pickHex(gradiant[0][1], gradiant[1][1], 1) // default
}

function pickHex (color1, color2, weight) {
  const p = weight
  const w = p * 2 - 1
  const w1 = (w / 1 + 1) / 2
  const w2 = 1 - w1
  const rgb = 'RGB(' + Math.round(color1[0] * w1 + color2[0] * w2) + ',' + Math.round(color1[1] * w1 + color2[1] * w2) + ',' +
      Math.round(color1[2] * w1 + color2[2] * w2) + ')'
  return rgb
}

function calculateDistance (pointA, pointB) {
  return Math.sqrt((pointB.x - pointA.x) * (pointB.x - pointA.x) + (pointB.y - pointA.y) * (pointB.y - pointA.y))
}

export function thirdPointFrom2Point (pointA, pointB, distance = undefined) {
  let x = 0, y = 0, m = 0, b = 0
  if (pointA.x !== pointB.x) {
    m = (pointB.y - pointA.y) / (pointB.x - pointA.x)
  }
  b = pointA.y - m * pointA.x

  if (!distance) {
    distance = calculateDistance(pointA, pointB)
  }

  if (pointB.x > pointA.x) {
    x = pointB.x + distance * Math.cos(Math.atan(m))
  } else {
    x = pointA.x - distance * Math.cos(Math.atan(m))
  }
  y = m * x + b

  return { x: x, y: y }
}

// COMPUTE A FORCED LAYOUT
export function computeForcedLayout (pinnedNodeID, graph, nodes) {
  const numberOfIterations = 5000
  const forcedLayout = Viva.Graph.Layout.forceDirected(graph, {
    springCoeff: 0.0008,
    gravity: -100,
    springTransform: (link, spring) => {
      spring.length = link.data.linkLength
    }
  })
  if (graph.getNode(pinnedNodeID)) forcedLayout.pinNode(graph.getNode(pinnedNodeID), true)

  // Radial case
  graph.forEachNode(function (node) {
    node.links.forEach((link) => {
      if (link.toId === node.id) {
        const positionTo = forcedLayout.getNodePosition(link.toId)
        const positionFrom = forcedLayout.getNodePosition(link.fromId)
        const rotation = link.data.rotation
        const radius = link.data.linkLength
        const pos = {
          x: radius * Math.cos(rotation * 2 * Math.PI / 180),
          y: radius * Math.sin(rotation * 2 * Math.PI / 180)
        }
        positionTo.x = positionFrom.x + pos.x
        positionTo.y = positionFrom.y + pos.y
      }
    })
  })

  for (let i = 0; i < numberOfIterations / 2; i++) {
    forcedLayout.step()
  }

  // Displace relative to center and parent position
  const positionNodeZero = forcedLayout.getNodePosition(pinnedNodeID)
  const nodeZero = graph.getNode(pinnedNodeID)
  let forDepth = nodeZero.data.depth - 1
  const displaced = Array(nodes.length).fill(false)
  const isDisplaced = []

  do {
    nodes.forEach((node) => {
      if (node.depth === forDepth) {
        // const n = graph.getNode(node.id)
        const position = forcedLayout.getNodePosition(node.id)

        node.links.forEach((link) => {
          if (!displaced[link.id]) {
            displaced[link.id] = true
            const nodelink = graph.getNode(link.id)
            const linkPosition = forcedLayout.getNodePosition(link.id)
            const newLinkPosition = thirdPointFrom2Point(positionNodeZero, position,
              calculateDistance(position, linkPosition) + calculateDistance(position, positionNodeZero))
            linkPosition.x = newLinkPosition.x
            linkPosition.y = newLinkPosition.y
            isDisplaced.push(nodelink.id)
          }
        })

        if (!displaced[node.id]) {
          displaced[node.id] = true
          const newPosition = thirdPointFrom2Point(positionNodeZero, position)
          position.x = newPosition.x
          position.y = newPosition.y
          isDisplaced.push(node.id)
        }
      }
    })
    forDepth--
  } while (forDepth > 0)

  for (let i = 0; i < numberOfIterations / 2; i++) {
    forcedLayout.step()
  }

  // SET A CONSTANT LAYOUT (using positions computed before)
  const layout = Viva.Graph.Layout.constant(graph)
  layout.placeNode(node => {
    return forcedLayout.getNodePosition(node.id)
  })
  // Set a pinned node
  layout.pinNode(graph.getNode(pinnedNodeID), true)

  // return forcedLayout
  return layout
}
