
// TRANSFORMADA PARA ALEJAR NODOS LEJANOS A UN VALOR MAS SIGNIFICATIVO
function f (m) {
  return m * m * m
}

// TRANSFORMADA CAMBIA VALOR DE DOMINIO ENTRE VALORES min-max A VALORES ENTRE sizeMin-sizeMax
export function transform (m, min, max, sizeMin, sizeMax) {
  return Math.floor(sizeMin + ((sizeMax - sizeMin) / (max - min)) * (m - min))
}

export function scale (mm, k1 = undefined, k2 = undefined) {
  const [min, max] = minMax(mm)
  const arr = Array(mm.length).fill().map(() => Array(mm.length).fill(0))
  mm.forEach((row, i) => {
    row.forEach((item, j) => {
      let l = (i !== j) ? transform(f(item), f(min), f(max), 1, 100) : 0
      if (k1 !== undefined && l < k1) l = -1
      if (k2 !== undefined && l > k2) l = -1
      arr[i][j] = l
    })
  })
  return arr
}

export function minMax (mm) {
  const arr = [].concat.apply([], mm)

  let min = Number.POSITIVE_INFINITY
  let max = Number.NEGATIVE_INFINITY

  arr.forEach((dist) => {
    if (min > dist && dist !== 0) min = dist
    if (max < dist && dist !== 0) max = dist
  })

  return [min, max]
}

// HISTOGRAMA, MEDIA, MEDIANA, DESVIACION-ESTANADAR PARA ANALISIS Y CARACTERIZACION DE LOS DATOS
// NO UTILIZADA PARA SOLUCION
export function histogram (mm, len, min, max) {
  const hist = Array(len).fill(0)
  const histzero = Array(len).fill(0)
  mm[0].forEach((m) => {
    if (m >= min && m <= max) {
      const i = transform(m, min, max, 0, len - 1)
      histzero[i]++
    }
  })
  // console.log("histzero", histzero)

  const arr = []
  mm.forEach((row, i) => {
    if (row instanceof Array) {
      for (let j = i + 1; j < row.length; j++) {
        arr.push(row[j])
      }
    } else {
      arr.push(row)
    }
  })

  let sum = 0
  let count = 0
  arr.forEach((m) => {
    if (m >= min && m <= max) {
      const i = transform(m, min, max, 0, len - 1)
      hist[i]++
      sum += m
      count++
    }
  })

  const arrSorted = arr.sort((a, b) => Number.parseFloat(a) - Number.parseFloat(b))
  // console.log('arrSorted', arrSorted);
  const mean = sum / count
  let sumvar = 0
  arrSorted.forEach((m) => {
    if (m >= min && m <= max) {
      sumvar += (m - mean) * (m - mean)
    }
  })
  const variance = sumvar / count
  const stdDeviation = Math.sqrt(variance)
  const median = arrSorted[Math.floor(arrSorted.length / 2)]

  return [hist, mean, stdDeviation, median]
}

export function sortByID (nodes) {
  const tmpNodes = nodes.map((node) => {
    const links = node.links.sort((la, lb) => la.id - lb.id)
    const tmpNode = {}
    Object.assign(tmpNode, node)
    tmpNode.links = links
    return tmpNode
  })
  const sorted = tmpNodes.sort((a, b) => a.id - b.id)
  return sorted
}

export function sortByM (nodes) {
  const tmpNodes = nodes.map((node) => {
    const links = node.links.sort((la, lb) => {
      if (la.m === lb.m) return la.id - lb.id
      return la.m - lb.m
    })
    const tmpNode = {}
    Object.assign(tmpNode, node)
    tmpNode.links = links
    return tmpNode
  })
  const sorted = tmpNodes.sort((a, b) => {
    for (let i = 0; i < a.links.length && i < b.links.length; i++) {
      if (a.links[i].m !== b.links[i].m) {
        return a.links[i].m - b.links[i].m
      }
    }
    return a.links.length - b.links.length
  })
  return sorted
}

export function findDepthOnLinks (nodesByID, orderArray, isAdded, links) {
  links.forEach((link, i) => {
    if (!isAdded[link.id]) {
      isAdded[link.id] = true
      const node = nodesByID[link.id]
      if (node !== undefined) {
        orderArray.push(node)
        if (node.links !== []) findDepthOnLinks(nodesByID, orderArray, isAdded, node.links)
      }
    }
  })
}

export function sortByDepth (nodesByID) {
  const nodesWithDepths = nodesByID.map((node) => {
    const tmpNode = {}
    Object.assign(tmpNode, node)
    tmpNode.depth = undefined
    calculateDepth(tmpNode, nodesByID)
    return tmpNode
  })
  const tmpNodes = nodesWithDepths.map((node) => {
    const links = node.links.sort((la, lb) => nodesByID[lb.id].depth - nodesByID[la.id].depth)
    const tmpNode = {}
    Object.assign(tmpNode, node)
    tmpNode.links = links
    return tmpNode
  })
  return tmpNodes.sort((a, b) => b.depth - a.depth)
}

function calculateDepth (node, nodesByID) {
  if (node.links.length === 0) {
    node.depth = 0
    return 0
  }
  const depths = Array(node.links.length).fill()
  node.links.forEach((link, i) => {
    depths[i] = calculateDepth(nodesByID[link.id], nodesByID)
  })
  node.depth = Math.max(...depths) + 1
  return node.depth
}
